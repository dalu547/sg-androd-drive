package com.seneca.global.room;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.seneca.global.models.Inbox;
import com.seneca.global.typeconverters.DateTypeConverter;

@Database(entities = {Inbox.class}, version = 1,exportSchema = false)
@TypeConverters({DateTypeConverter.class})
public abstract class InboxDatabase extends RoomDatabase {
    private static InboxDatabase instance;

    public abstract InboxDao inboxDao();

    public static synchronized InboxDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    InboxDatabase.class, "mailbox_table")
                    .fallbackToDestructiveMigration()
//                    .addCallback(roomCallback)
                    .build();

        }
        return instance;
    }
    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDBAsyncTask(instance).execute();
        }
    };
    private static class PopulateDBAsyncTask extends AsyncTask<Void,Void,Void>{
          private InboxDao inboxDao;
          private PopulateDBAsyncTask(InboxDatabase inboxDatabase){
           inboxDao=inboxDatabase.inboxDao();
          }
        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }


}
