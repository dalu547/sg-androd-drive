package com.seneca.global.room;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.seneca.global.models.Inbox;

import java.util.Date;
import java.util.List;

public class InboxRepository {
    private InboxDao inboxDao;
    private LiveData<List<Inbox>> inboxList;

    public InboxRepository(Application application) {
        InboxDatabase database = InboxDatabase.getInstance(application);
        inboxDao = database.inboxDao();
        inboxList = inboxDao.getInboxList();

    }

    public void insert(Inbox inbox) {
        new InsertMailAsyncTask(inboxDao).execute(inbox);
    }

    public void delete(Inbox inbox) {
        new DeleteMailAsyncTask(inboxDao).execute(inbox);

    }

    public LiveData<List<Inbox>> getInboxList() {
        return inboxList;
    }

    private static class InsertMailAsyncTask extends AsyncTask<Inbox, Void, Void> {
        private InboxDao inboxDao;

        private InsertMailAsyncTask(InboxDao inboxDao) {
            this.inboxDao = inboxDao;
        }

        @Override
        protected Void doInBackground(Inbox... inboxes) {
            inboxDao.insert(inboxes[0]);
            return null;
        }
    }


    private static class DeleteMailAsyncTask extends AsyncTask<Inbox, Void, Void> {
        private InboxDao inboxDao;

        private DeleteMailAsyncTask(InboxDao inboxDao) {
            this.inboxDao = inboxDao;
        }

        @Override
        protected Void doInBackground(Inbox... inboxes) {
            inboxDao.delete(inboxes[0]);
            return null;
        }
    }

}
