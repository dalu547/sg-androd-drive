package com.seneca.global.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.seneca.global.models.Inbox;

import java.util.List;

@Dao
public interface InboxDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert (Inbox inbox);

    @Delete
    void delete (Inbox inbox);

    @Query("SELECT * FROM mailbox_table")
    LiveData<List<Inbox>> getInboxList();
}
