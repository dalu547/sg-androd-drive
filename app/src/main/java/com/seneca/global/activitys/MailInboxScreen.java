package com.seneca.global.activitys;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.annotation.Nullable;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ProgressBar;
import com.seneca.global.R;
import com.seneca.global.adapters.InboxAdapter;
import com.seneca.global.models.Inbox;
import com.seneca.global.viewmodels.InboxViewModel;

import java.util.ArrayList;
import java.util.List;

public class MailInboxScreen extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    private InboxAdapter mAdapter;
    private ProgressBar mProgressBar;
    private List<Inbox> inboxList = new ArrayList<>();
    private InboxViewModel mInboxViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recyclerView);
        mProgressBar = findViewById(R.id.progressBar);
        showProgressBar();
        mAdapter = new InboxAdapter(this, inboxList);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mInboxViewModel = ViewModelProviders.of(this).get(InboxViewModel.class);

        mInboxViewModel.makeApiCall();
        displaydata();
    }


    public void displaydata() {

        if (!isNetworkAvailable(MailInboxScreen.this)) {
            mInboxViewModel.getInboxList().observe(this, new Observer<List<Inbox>>() {
                @Override
                public void onChanged(@Nullable List<Inbox> list) {

                    inboxList=list;
                    mAdapter.refresh(inboxList);
                    hideProgressBar();
                }
            });
        } else {


            mInboxViewModel.getInboxMailsObserver().observe(this, new Observer<List<Inbox>>() {
                @Override
                public void onChanged(@Nullable List<Inbox> inboxes) {

                    inboxList=inboxes;
                    mAdapter.refresh(inboxList);
                    hideProgressBar();

                }
            });

        }


    }

    public static boolean isNetworkAvailable(Context context) {
        return ((ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo() != null;
    }


    private void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }


}




















