package com.seneca.global.activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.seneca.global.R;


public class LoginScreen extends Activity {
    private EditText etPassword, etEmail;
    private Button btnLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        initializeControls();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = etEmail.getText().toString().trim();
                String password = etPassword.getText().toString().trim();

                String errorMsg = validateLogin(email, password);
                if (errorMsg.isEmpty()) {
                    SharedPreferences sharedpreferences = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("IS_LOGGEDIN", true);
                    editor.commit();
                    Intent intent = new Intent(LoginScreen.this, MailInboxScreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(LoginScreen.this, getString(R.string.login_successfully), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.valid_credentials), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    private String validateLogin(String username, String password) {

        String errorMsg = "";
        if (username.isEmpty()) {
            errorMsg = getString(R.string.username);
        } else if (password.isEmpty()) {
            errorMsg = getString(R.string.password);
        }
        return errorMsg;
    }

    public void initializeControls() {
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
    }

    public void register(View view) {
        Toast.makeText(this, "Register", Toast.LENGTH_SHORT).show();
    }
}











