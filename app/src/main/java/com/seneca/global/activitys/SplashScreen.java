package com.seneca.global.activitys;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.seneca.global.R;

public class SplashScreen extends Activity {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.splash_screen);
            int secondsDelayed = 3;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    SharedPreferences preference= getSharedPreferences("PREFERENCE", 0);
                    boolean loggedin = preference.getBoolean("IS_LOGGEDIN", false);
                    if(loggedin){
                        startActivity(new Intent(SplashScreen.this, MailInboxScreen.class));
                        finish();
                    }else {
                        startActivity(new Intent(SplashScreen.this, LoginScreen.class));
                        finish();
                    }

                }
            }, secondsDelayed * 1000);
        }

}
