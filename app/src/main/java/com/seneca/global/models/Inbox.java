package com.seneca.global.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "mailbox_table")
public class Inbox {


    public String picture;
    public String subject;
    public String message;
    public String timestamp;
    public boolean isRead;
    public boolean isImportant;

    @NonNull
    @PrimaryKey
    public String from;
}
