package com.seneca.global.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.seneca.global.models.Inbox;
import com.seneca.global.network.APIService;
import com.seneca.global.network.RetroInstance;
import com.seneca.global.room.InboxRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InboxViewModel extends AndroidViewModel {

    private MutableLiveData<List<Inbox>> mailList;
    private LiveData<List<Inbox>> inboxes;
    InboxRepository repository;

    public InboxViewModel(@NonNull Application application) {
        super(application);

        mailList = new MutableLiveData<>();
        repository = new InboxRepository(application);
        inboxes = repository.getInboxList();
    }


    public MutableLiveData<List<Inbox>> getInboxMailsObserver() {
        return mailList;
    }

    public void makeApiCall() {
        APIService apiService = RetroInstance.getRetroClient().create(APIService.class);
        Call<List<Inbox>> call = apiService.getInbox();
        call.enqueue(new Callback<List<Inbox>>() {
            @Override
            public void onResponse(Call<List<Inbox>> call, Response<List<Inbox>> response) {
                mailList.postValue(response.body());
                mailList.setValue(response.body());
                List<Inbox> listValue = mailList.getValue();
                insertDB(listValue);

            }

            @Override
            public void onFailure(Call<List<Inbox>> call, Throwable t) {
                mailList.postValue(null);
            }
        });
    }

    public void insertDB(List<Inbox> inboxes) {
        for (int i = 0; i < inboxes.size(); i++)
            repository.insert(inboxes.get(i));
    }

    public LiveData<List<Inbox>> getInboxList() {
        return inboxes;
    }

}
