package com.seneca.global.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.seneca.global.R;
import com.seneca.global.models.Inbox;

import java.util.ArrayList;
import java.util.List;


public class InboxAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Inbox> mInboxes ;
    private Context mContext;

    public InboxAdapter(Context context, List<Inbox> inboxes) {
        mInboxes = inboxes;
        mContext = context;
    }

    public void refresh(List<Inbox> inboxes) {
        this.mInboxes = inboxes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((ViewHolder) viewHolder).tvFrom.setText(mInboxes.get(i).from);
        ((ViewHolder) viewHolder).tvSubject.setText(mInboxes.get(i).subject);
        ((ViewHolder) viewHolder).tvMessage.setText(mInboxes.get(i).message);
        ((ViewHolder) viewHolder).tvTimestamp.setText("Received at : "+mInboxes.get(i).timestamp);
        RequestOptions defaultOptions = new RequestOptions()
                .error(R.drawable.senecaglobal);

        Glide.with(mContext)
                .setDefaultRequestOptions(defaultOptions)
                .load(mInboxes.get(i).picture)
                .into(((ViewHolder) viewHolder).mImage);
    }

    @Override
    public int getItemCount() {
        return mInboxes != null ? mInboxes.size() : 0;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImage;
        private TextView tvFrom,tvSubject,tvMessage,tvTimestamp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.image);
            tvFrom = itemView.findViewById(R.id.tvFrom);
            tvSubject = itemView.findViewById(R.id.tvSubject);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvTimestamp = itemView.findViewById(R.id.tvTimestamp);

        }
    }
}
