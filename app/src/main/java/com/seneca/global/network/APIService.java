package com.seneca.global.network;

import com.seneca.global.models.Inbox;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIService {
    @GET("inbox.json")
    Call<List<Inbox>> getInbox();
}
